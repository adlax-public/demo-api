FROM node:15
COPY . /app
WORKDIR /app
RUN npm install
EXPOSE 8000
CMD ["npm", "start"];